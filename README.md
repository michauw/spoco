# README #

SpoCo - modern web interface for spoken corpora
Version 2.0

## Settings ##

### init.php ###

TODO

### pattrs.json ###

A file that defines positional attributes of a corpus (word-level attributes). At least one attribute (called 'word') is required. 
This file is used in three ways:
* to specify fields for querying a corpus
* to define layers of presentation (e.g. standard and dialect ones)
* to connect attributes used in the interface and their counterparts stored in a CWB corpus
 
Structure:
* an array of attributes
* each attribute is represented by the object with following keys and values:
    * name (required): name of the p-attribute used in the CWB corpus
    * field (required): type of the field, it can be either 'text' or 'checkbox'
    * hint (required): 'human' name of the field (e.g. Word, Lemma etc.)
    * cwb (required): position (starting from 0) of the attribute in the CWB corpus ('word' is always 0)
    * initialValue (optional): default value of the field (it should be used for 'checkbox' type only)
    * valueOn (required if the field is 'checkbox'): value that will be used in a query constructor when this field is set to *true*
    * valueOff (optional, used only in the 'checkbox' fields): value that will be used in a query constructor when this field is set to *false*
    * layer (optional): number of the layer, used to specify that this attribute should be used for displaying given layer
    * layerName (required if the *layer* option is set for this attribute): name of the layer (e.g. 'standard' or 'dialectal')
    
### meta.json ###

TODO

### meta_data.json ###

TODO
 

### Who do I talk to? ###

* (c) Michał Woźniak (michauwww@gmail.com)