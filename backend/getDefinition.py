# -*- coding: utf-8 -*-

import sqlite3
import re
import sys
import traceback

def regexp(expr, item):
    reg = re.compile(expr, re.U)
    return reg.search(item) is not None

def insertLineBreaks (results):
    for i in range (len (results)):
        for el in [-3, -2]:     # definition, etymology
            results[i][el] = re.sub (r'\r?\n', '<br>', results[i][el])
    return results

def sameVariant (v1, v2):
    for i in range (len (v1)):
        if i == 2:  # pronunciation
            continue
        if v1[i].decode ('utf8').strip () != v2[i].decode ('utf8').strip ():
            return False
    return True

def mergePronVariants (results):
    out = []
    for i in range (1, len (results)):
        for j in range (i):
            if sameVariant (results[j], results[i]):
                results[j][2] += ', ' + results[i][2]
                out.append (i)
                break
    return [results[i] for i in range (len (results)) if not i in out]
try:
    database = 'dyferencyjne.db'
    fields = ['entry', 'pronunciation', 'part_of_speech', 'definition', 'etymology', 'inflection']
    attr_fields = ['zwrotnosc', 'wlasciwy', 'przechodniosc', 'aspekt', 'rodzaj']

    word = sys.argv[1].decode ('utf8')
    mode = sys.argv[2]

    conn = sqlite3.connect (database)
    conn.create_function("REGEXP", 2, regexp)
    cur = conn.cursor ()
    patt = r"(^|, ?)%s(,|$)" % word
    sql_fields = ','.join (['id'] + fields)
    sql = 'SELECT DISTINCT %s FROM lexeme WHERE attested_forms REGEXP ? OR entry REGEXP ?' % sql_fields
    # print (sql)
    cur.execute (sql, (patt, patt))
    response = cur.fetchall ()
    ids = [int (r[0]) for r in response]
    response = [r[1:] for r in response]
    results = [[r.encode ('utf8') for r in row] for row in response]
    sql_attr = 'SELECT %s FROM lexeme_attributes WHERE lexeme_id=%d'
    for ind in range (len (ids)):
        try:
            zwrotnosc, wlasciwy, przechodniosc, aspekt, rodzaj = [el.encode ('utf8') for el in cur.execute (sql_attr % (','.join (attr_fields), ids[ind])).fetchone ()]
        except TypeError:
            zwrotnosc, wlasciwy, przechodniosc, aspekt, rodzaj = [''] * 5
        results[ind].insert (1, (' ' if zwrotnosc else '') + zwrotnosc)
        zwrotnosc_gw = zwrotnosc.replace ('(się)', '(sie)').replace ('sobie', 'se')
        if zwrotnosc_gw:
            zwrotnosc_gw = (' ' if mode == 'raw' else '&nbsp;') + zwrotnosc_gw
        results[ind].insert (3, zwrotnosc_gw)
        characteristic = ' '.join ([el for el in [wlasciwy, przechodniosc, aspekt, rodzaj] if el.strip ()])
        results[ind].insert (5, characteristic)

    results = mergePronVariants (results)
    results = insertLineBreaks (results)

    etymology_templ = '''<div class="definition etymology">
        <b>Etymology:</b> %s
    </div>'''
    for i, res in enumerate (results):
        if mode == 'html':
            res = res[:-1]
            res[0] = res[0].decode ('utf8').upper ().encode ('utf8')
            res[1] = res[1].decode ('utf8').upper ().encode ('utf8')
            templ = '''<div class="headword definition"><b>%s %s</b> [%s%s]</div>
                        <div class="definition pos">%s %s</div>
                        <div class="definition">
                            %s
                        </div>
                        %s
                        '''
            try:
                ind = -1
                if res[ind]:
                    res[ind] = etymology_templ % res[ind]
            except ValueError:
                pass

            if i < len (results) - 1:
                templ += '\n<hr>\n'
            try:
                print templ % tuple (res)
            except IndexError:
                print '<i>No definition</i>'
        elif mode == 'raw':
            print '\t'.join ([r.strip () for r in res])
except Exception as e:
    tmp1 = sys.stdin
    tmp2 = sys.stderr
    with open ('debug/getDefinition_err.txt', 'w') as fout:
        sys.stdin = fout
        sys.stderr = fout
        print type (e)
        print # -*- coding: utf-8 -*-
        traceback.print_exc ()
        print sys.argv[1]
        print sys.argv[2]
    sys.stdin = tmp1
    sys.stderr = tmp2
