# -*- coding: utf-8 -*-

import sqlite3
import re
import sys

ordering_field = 0

def pl_alpha_order (el1, el2):
    order = 'aąbcćdeęfghijklłmnńoópqrsśtuvwxyzźż'.decode ('utf8')
    # print el1, type (el1)
    # print el2, type (el2)
    for a, b in zip (el1, el2):
        try:
            apos = order.index (a.lower ())
        except ValueError:
            apos = 100
        try:
            bpos = order.index (b.lower ())
        except ValueError:
            bpos = 100
        if apos < bpos:
            return -1
        elif bpos < apos:
            return 1
    if len (el1) == len (el2):
        return 0
    elif len (el1) < len (el2):
        return -1
    else:
        return 1


database = 'dyferencyjne.db'
fields = ['entry']#, 'pronunciation', 'part_of_speech', 'definition', 'etymology']

l = sys.argv[1]
match = sys.argv[1].decode ('utf8')
expand = sys.argv[2]

if 'l' in expand:
    match = '%' + match
if 'r' in expand:
    match += '%'

conn = sqlite3.connect (database)
cur = conn.cursor ()
sql_fields = ','.join (fields)
sql = 'SELECT DISTINCT %s FROM lexeme WHERE entry LIKE "%s"' % (sql_fields, match)
cur.execute (sql)
results = sorted (cur.fetchall (), cmp = lambda x, y: pl_alpha_order (x[ordering_field], y[ordering_field]))
results = [[r.encode ('utf8') for r in row] for row in results]
print '\n'.join ([el[0] for el in results if el[0].strip ()]),
