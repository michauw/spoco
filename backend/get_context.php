<?php
    include ('../settings/init.php');
    $contextSize = 3;
    $params = json_decode (file_get_contents ('php://input'), true);
    if ($params['corpus'] == 'main') {
        $corpus = "$CORPUSNAME;";
    }
    elseif ($params['corpus'] == 'full') {
        $corpus = "$CORPUSNAME_FULL;";
    }
    $from = $params['location']['from'];
    $to = $params['location']['to'];
    $file = $params['location']['file'];
    if ($DEBUG == true) {
        file_put_contents ('debug/comm_context.txt', "\n"); // DEBUG
        file_put_contents ('debug/context_results.txt', "\n");
    }
    $show = 'show';
    foreach ($pattrs as $pattr) {
        if ($pattr->name != "word") {
            $show .= " +" . $pattr->name;
        }
    }
    $show .= ";";
    $printStructures = "utterance_id, utterance_spkr, utterance_to, utterance_from, utterance_file";
    if (strlen ($ANNOTCONTEXT) > 0) {
        $printStructures .= ', ' . $ANNOTCONTEXT;
    }           
    $pstructures = "set ShowTagAttributes on; set PrintStructures '$printStructures';";
    $context = 'set Context 1 utterance;';
    $query_central = "<utterance_from='$from'><utterance_to='$to'><utterance_file='$file'> [];";
    $command_initial = "$CWBDIR" . "cqpcl -r $REGISTRY \"$corpus $pstructures $show $context $query_central\"";
    exec ($command_initial, $central);
    $central = implode ("\n", $central);
    if ($DEBUG == true)
        file_put_contents ('debug/comm_context.txt', $command_initial . "\n\n", FILE_APPEND); // DEBUG
    preg_match ('/<utterance_id (\d+)/', $central, $matches);
    $id = $matches[1];
    for ($i = -$contextSize; $i <= $contextSize; ++$i) {
        if ($i == 0) {
            echo $central . "\n";
            continue;
        }
        $query = "<utterance_id='" . ($id + $i) . "'> [];";
        $command = "$CWBDIR" . "cqpcl -r $REGISTRY \"$corpus $pstructures $show $context $query\"";
        exec ($command, $results);
        $results = implode ("\n", $results);
        if ($DEBUG == true)
            file_put_contents ('debug/context_results.txt', $results . "\n", FILE_APPEND);
        echo $results;
        if ($i < $contextSize) {
            echo "\n";
            }
            if ($DEBUG == true)
                file_put_contents ('debug/comm_context.txt', $command . "\n", FILE_APPEND); // DEBUG
       }
?>
