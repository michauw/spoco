<?php
    include ('../settings/init.php');
    $params = json_decode (file_get_contents ('php://input'), true);
	$init_mode = $params['init'];
    $meta = '';
    if ($params['corpus'] == 'main') {
        $corpus = "$CORPUSNAME;";
    }
    elseif ($params['corpus'] == 'full') {
        $corpus = "$CORPUSNAME_FULL;";
    }
    $context = 'set Context 1 utterance;';
    $query = $params['query'];
    if ($DEBUG == true)
        file_put_contents ('debug/query.txt', $query); // DEBUG
    $count = 'size Last;';
	$interval = 500;
    $show = 'show';
    foreach ($pattrs as $pattr) {
        if ($pattr->name != "word") {
            $show .= " +" . $pattr->name;
        }
    }
    $show .= ";";    
    $query .= '; ';
    $pstructures = "set ShowTagAttributes on; set PrintStructures 'utterance_id, utterance_from, utterance_to, utterance_file";
    if (strlen ($ANNOTCONTEXT) > 0) {
        $pstructures .= ', ' . $ANNOTCONTEXT;
    }
    $pstructures .= "';";
	$query_initial = "A=$query";
    $command_initial = "$CWBDIR" . "cqpcl -r $REGISTRY \"$corpus $context $show $query_initial $count\"";
    if ($DEBUG == true)
        file_put_contents ('debug/comm_init.txt', $command_initial); // DEBUG
	exec ($command_initial, $number_results);
    if ($DEBUG == true)
        file_put_contents ('debug/nres.txt', $number_results);
	if ($init_mode) {
		echo $query . "\n";
		$resNumb = intval ($number_results[0]);
		echo $number_results[0];
	}
	else {
		$resNumb = intval ($number_results[0]);
		if ($resNumb <= $interval) {
			$command = "$CWBDIR" . "cqpcl -r $REGISTRY \"$corpus $pstructures $context $show $query\"";
			exec ($command, $results);
			$results = implode ("\n", $results);
            if ($DEBUG == true) {
                file_put_contents ('debug/res.txt', $results);    // DEBUG:
                file_put_contents ('debug/comm.txt', $command);   // DEBUG:
            }
			echo $results;
		}
		else {
			for ($i = 0; $i <= $resNumb; $i += $interval) {
				$j = $i + $interval;
				$cat = "cat A $i $j;";
				$command = "$CWBDIR" . "cqpcl -r $REGISTRY \"$corpus $pstructures $context $show $query_initial $cat\"";
				exec ($command, $results);
				$results = implode ("\n", $results);
				echo $results;
				echo "\n";

			}
            if ($DEBUG == true) {
                file_put_contents ('debug/res.txt', $results);    // DEBUG:
                file_put_contents ('debug/comm.txt', $command);   // DEBUG:
            }
		}
	}
?>
