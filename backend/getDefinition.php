<?php
    include ('../settings/init.php');
    $params = json_decode (file_get_contents ('php://input'), true);
    if ($DEBUG == true)
        file_put_contents ('debug/getDefinition_params.txt', $params);
    $word = $params['word'];
    $mode = $params['mode'];
    $command = "python getDefinition.py '$word' $mode";
    if ($DEBUG == true)
        file_put_contents ('debug/getDefinitions_command.txt', $command);
    $results = shell_exec ($command);
    if ($DEBUG == true)
        file_put_contents ('debug/getDefinition_output.txt', $results);
    echo $results;
?>
