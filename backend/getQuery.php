<?php
    include ('../settings/init.php');
    $params = json_decode (file_get_contents ('php://input'), true);
    $query = $params['query'];
    $expand = $params['expand'];
    $command = "python getQuery.py '$query' $expand";
    if ($DEBUG == true)
        file_put_contents ('debug/getQuery_comm.txt', $command);
    $results = shell_exec ($command);
    if ($DEBUG == true)
        file_put_contents ('debug/getQuery_output.txt', trim ($results));
    echo trim ($results);
?>
