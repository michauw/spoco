var spoco = angular.module ('spoco', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'gettext', 'angularjs-dropdown-multiselect', 'ngDialog' , 'ui.bootstrap', 'ui.bootstrap.pagination', 'ngMaterial', 'rwdImageMaps']);

spoco.config (function ($routeProvider) {
        $routeProvider
        .when ('/', {
            templateUrl: 'jsapp/pages/corpus.html',
            controller: 'corpus',
            resolve: {
                        pattrs: function ($http) {
                                        return $http.get ('settings/pattrs.json').then (function (response) {
                                            return response.data;
                                        });
                                    },
                        metaFields: function ($http) {
                                        return $http.get ('settings/meta.json').then (function (response) {
                                            return response.data;
                                        });
                                    },
                        metaValues: function ($http) {
                                        return $http.get ('settings/meta_data.json').then (function (response) {
                                            return response.data;
                                        });
                        }
                    }
        })        
        .when ('/help', {
            templateUrl: 'jsapp/pages/help.html',
            controller: 'help',
            resolve: {
                        helpBoxes: function ($http) {
                            return $http.get ('jsapp/pages/help/help.txt').then (function (response) {
                                        return response.data;
                                    });
                        }
            }
        })
        .when ('/info', {
            templateUrl: 'jsapp/pages/info.html',
            controller: 'info'
        })
        .when ('/dictionary', {
            templateUrl: 'jsapp/pages/dictionary.html',
            controller: 'info'
        })
        .when ('/results', {
            templateUrl: 'jsapp/pages/results.html',
            controller: 'results'
        })
        .otherwise ({redirectTo:'/'});
});

spoco.run (function (gettextCatalog) {
    gettextCatalog.debug = true;
});


spoco.controller ('header', ['$scope', 'gettextCatalog', '$location', function ($scope, gettextCatalog, $location) {
        var links = {'pl':
                        {
                            'help': 'pomoc',
                            'info': 'info',
                            'dictionary': 'slownik'
                        }
            };
        $scope.curLang = 'en';
        $scope.switchLanguage  = function (lang) {
            $scope.curLang = lang;
            // reloadLang ();
            gettextCatalog.setCurrentLanguage (lang);
        }

        $scope.getLink = function (ln) {
            var language = gettextCatalog.getCurrentLanguage ();
            if (language != 'pl' && links[language] != undefined)
                ln = links[language][ln];
            return '#!/' + ln;
        }
}]);

spoco.controller ('info', ['$scope', function ($scope) {
    console.log ('info ctrl');
    $scope.abc = 'pl';
}]);

