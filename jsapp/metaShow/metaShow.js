spoco.directive ('metaShow', ['metaKeeper', function (metaKeeper) {
    return {
        templateUrl: 'jsapp/metaShow/metaShow.html',
        restrict: 'E',
        scope: {
            show: '=',
            content: '=',
            fieldsToShow: '='
        },
        controller: function($scope) {
            
            var getHumanName = function (name) {
                var initial = /^(meta|utterance)_/;
                var part = /([^\W_]+)/g;
                var converted = [];
                name = name.replace (initial, '');
                while ((match = part.exec (name)) != null) {
                    converted.push (match[1]);
                }
                converted = converted.join (' ');
                converted = converted[0].toUpperCase () + converted.slice (1);
                
                return converted;
            }
            
            $scope.metaToShow = [];
            for (var i = 0; i < $scope.fieldsToShow.length; ++i) {
                var field = $scope.fieldsToShow[i];
                if (field.inResults)
                    $scope.metaToShow.push ({name: field.hint, id: getHumanName (field.name)});
            }
        }
    }
}])
