spoco.directive ('dictionary', ['$http', '$location', 'queryKeeper', 'dictKeeper', function ($http, $location, queryKeeper, dictKeeper) {
    return {
            templateUrl: 'jsapp/dictionary/dictionary.html',
            restrict: 'E',
            scope: {
                language: '@language'
            },
            controller: function ($scope, $http, $location, queryKeeper, dictKeeper) {
                console.log ($scope.language);
                $scope.indexClass = {outer: 'my-10',
                                     index: 'col-md-8 offset-md-2',
                                     search: 'col-md-4 offset-md-4 my-3',
                                     input: 'form-control inline',
                                     icon: 'fa fa-search'
                                 };
                $scope.active_letter = '';
                $scope.langLetters = {
                    'en': ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
                    'pl': ['A', 'B', 'C', 'Ć', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'O', 'P', 'R', 'S', 'Ś', 'T', 'U', 'W', 'Z', 'Ź', 'Ż']
                }
                $scope.letters = $scope.langLetters[$scope.language];
                $scope.entries = [];
                $scope.paged_entries = [];
                $scope.showList = false;
                $scope.showEntry = '';
                $scope.noResults = '';
                $scope.pagination = {
                                        currentPage: 1,
                                        numPerPage: 25,
                                        maxSize: 5,
                                        boundaries: false
                                    };
                $scope.active = -1;
                $scope.last = '';

                queryKeeper.resetQuery ();

                var savedScope = dictKeeper.get ('scope');
                if (savedScope !== null)
                {
                    $scope.entries = savedScope.entries;
                    $scope.pagination = savedScope.pagination;
                    $scope.paged_entries = savedScope.paged_entries;
                    $scope.showEntry = savedScope.showEntry;
                    $scope.showList = savedScope.showList;
                    $scope.definitions = savedScope.definitions;
                    $scope.active_letter = savedScope.active_letter;
                    $scope.indexClass = savedScope.indexClass;
                }

                var getSlice = function (data) {
                    var begin = (($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage);
                    var end = begin + $scope.pagination.numPerPage;
                    return data.slice (begin, end);
                }

                $scope.showQuery = function (query, expand = '') {
                    $scope.noResults = false;
                    $scope.indexClass = {outer: 'mt-2 mb-4',
                                         index: 'col-md-6 offset-md-2 smaller-d',
                                         search: 'col-md-2',
                                         input: 'form-control smaller-d inline',
                                         icon: 'fa fa-search smaller-d'
                                     };
                    $scope.active_letter = query;
                    $scope.pagination.currentPage = 1;
                    dictKeeper.set ('query', {query: query, expand: expand});
                    $http.post ('backend/getQuery.php', {query: query, expand: expand}).then (function (response) {
                        $scope.entries = [];
                        var data = [];
                        if (response.data) {
                            data = response.data.split ('\n');
                            for (var i = 0; i < data.length; ++i) {
                                $scope.entries.push (data[i]);
                            }
                            $scope.paged_entries = getSlice ($scope.entries);
                            $scope.getEntry ($scope.entries[0]);
                            $scope.showList = true;
                        }
                        else {
                            $scope.noResults = true;
                            $scope.showList = false;
                            $scope.showEntry = '';
                        }
                        // $scope.showEntry = savedScope.showEntry;
                    });
                }
                $scope.showInResults = function (definitions) {
                    if (typeof (definitions) == 'undefined')
                        return 0;   
                    return (definitions.length > 0);
                }
                $scope.getEntry = function (entry) {
                    $http.post ('backend/getDefinition.php', {word: entry, mode: 'raw'}).then (function (response) {
                        $scope.definitions = [];
                        var lines = response.data.split ('\n');
                        for (var i = 0; i < lines.length; ++i) {
                            if (lines[i].trim () == '')
                                continue;
                            var elements = lines[i].split ('\t');
                            var def = {
                                        entry: elements[0],
                                        reflexiveness: elements[1],
                                        pronunciation: elements[2],
                                        reflexiveness_dial: elements[3],
                                        pos: elements[4],
                                        characteristic: elements[5],
                                        content: elements[6].split ('<br>'),
                                        etymology: elements[7],
                                        inflection: elements[8]
                                    };
                            if (def.reflexiveness_dial)
                                def.reflexiveness_dial = ' ' + def.reflexiveness_dial;
                            $scope.definitions.push (def);
                        }
                        $scope.showEntry = entry;
                    })
                    dictKeeper.set ('entry', entry);
                }

                $scope.showPaginator = function () {
                    if ($scope.pagination.numPerPage >= $scope.entries.length) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                $scope.pageChanged = function () {
                    $scope.paged_entries = getSlice ($scope.entries);
                }

                $scope.goToResults = function () {
                    var words = $scope.definitions[0].entry.split (' ');
                    console.log ('words:', words);
                    var inflection = $scope.definitions[0].inflection.split (' ');
                    console.log ('inflection:', inflection);
                    var mode = '';
                    for (var i = 0; i < words.length; ++i) {
                        if (inflection[i] == '1')
                            mode = 'token';
                        else if (inflection[i] == '2')
                            mode = 'lexeme';
                        else {
                            mode = 'lexeme';
                            words[i] = inflection[i];
                        }
                        queryKeeper.setQuery (i, words[i], mode, i == 0);
                    }
                    if (words.length == 1)
                        queryKeeper.setQuery (0, '[12]', 'differential', false);
                    else
                        queryKeeper.setQuery (0, true, 'mwe', false);
                    dictKeeper.set ('scope', $scope);
                    console.log (queryKeeper.prepareQuery ());
                    $location.path ('/results');
                }

                $scope.goToDefinition = function (entry) {
                    dictKeeper.set ('scope', $scope);
                    $scope.getEntry (entry);
                }
            }
        };
}]);

spoco.filter ('addRefs', function () {
    return function (item) {
        var ref = /(zob\.|zdrob\. od|zgrub\. od) +([A-ZĄĆĘŁŃÓŚŹŻ ]+.*)/;
        var words = /([A-ZĄĆĘŁŃÓŚŹŻ ]+)(,\s*|$)/g;
        var entry = ref.exec (item);
        if (entry) {
            var new_entry = entry[1] + ' ';
            while ((results = words.exec (entry[2])) !== null)
                new_entry += '<span class="go-to" ng-click="goToDefinition (\'' + results[1].toLowerCase () + '\')">' + results[1] + '</span>, ';
            new_entry = new_entry.slice (0, new_entry.length - 2);
            return new_entry;
        }
        else
            return item;
    };
});

spoco.directive('compile', ['$compile', function ($compile) {
  return function(scope, element, attrs) {
    scope.$watch(
      function(scope) {
        return scope.$eval(attrs.compile);
      },
      function(value) {
        element.html(value);
        $compile(element.contents())(scope);
      }
   )};
  }])