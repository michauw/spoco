spoco.controller ('corpus', ['$scope', 'queryKeeper', 'metaKeeper', 'pattrs', 'metaFields', 'metaValues', function ($scope, queryKeeper, metaKeeper, pattrs, metaFields, metaValues) {
	queryKeeper.resetQuery ();
    queryKeeper.setPAttrs (pattrs);
    $scope.metaFields = metaFields;
    metaKeeper.setFields (metaFields);
    metaKeeper.setValues (metaValues);
}]);

spoco.controller ('results', ['$scope', 'processResults', '$http', '$compile', 'metaKeeper', 'queryKeeper', function ($scope, processResults, $http, $compile, metaKeeper, queryKeeper) {

    var limit = 10;

    $scope.results = [];
    $scope.pagination = {
                            currentPage: 1,
                            numPerPage: 20,
                            maxSize: 5,
                            boundaries: false
                        };

    $scope.formattedSlice = [];
    $scope.selection;
    $scope.sorted = {column: -1, direction: -1};
    $scope.loaded = false;
    $scope.initials_loaded = false;
    $scope.showMeta = false;
    $scope.sorting = [];
    $scope.mode = 'plain';
    $scope.layer = null;
    $scope.next_layer = '';
    $scope.expanded = new Array ($scope.pagination.numPerPage).fill (0);
    $scope.playing = '';
    $scope.audio;
    $scope.showContext = [];
    $scope.currentSlice = [];
    $scope.metaToShow = metaKeeper.getAllFields ();
    
	var pattrs = queryKeeper.getPattrs ();
    var layers = [];
    var l_order = [];
    pattrs.forEach (function (attr) {
        if (attr.hasOwnProperty ('layer')) {
            layers.push ({'attr': attr.name, 'name': attr.layer_name});
            l_order.push (attr.layer);
        }
    });
    layers = l_order.map (function (element) {return layers[element]});
    if (!layers.length)
        layers = [{'attr': pattrs[0].name, 'name': 'standard'}];
    $scope.layer = layers[0];
    if (layers.length > 1)
        $scope.next_layer = layers[1].name;

    /**    LOCAL FUNCTIONS     */

    var getSlice = function () {
        var begin = (($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage);
        var end = begin + $scope.pagination.numPerPage;
        $scope.selection = new Array ($scope.results.length).fill (false);
        $scope.showContext = new Array ($scope.results.length).fill (false);
        return $scope.results.slice (begin, end);
    }

    // var formatSlice = function () {
    //     var rows = [];
    //     for (var i = 0; i < currentSlice.length; ++i) {
    //         var rowData = currentSlice[i];
    //         if ($scope.mode === 'kwic') {
    //             rows.push ({'row': rowData.content, 'meta': rowData.meta});
    //         }
    //         rows.push (rowData);
    //         }
    //     return rows;
    // }

    var getContext = function (location, index, elem) {
        var originalClasses = elem.attr ('classes');
        var spinnerClasses = 'fas fa-spinner'
        angular.element (elem).removeClass (originalClasses).addClass (spinnerClasses);
        var dtime = new Date ();
        var time = dtime.getTime ();
        processResults.get (false, location)
            .then (function (response) {
                var res = processResults.prepareContext (response.data);
                $scope.results[index].context = res.context;
                $scope.results[index].contextMatch = res.index;
                // $scope.formattedSlice = formatSlice ();
                elem.removeClass (spinnerClasses).addClass (originalClasses);
                $scope.showContext[index] = !$scope.showContext[index];
            }, function (response) {
                elem.removeClass (spinnerClasses).addClass (originalClasses);
                $scope.showContext[index] = !$scope.showContext[index];
            });
    }

    /**     SCOPE FUNCTIONS   */

    $scope.switchMode = function () {
        if ($scope.mode == 'kwic')
            $scope.mode = 'plain';
        else {
            $scope.mode = 'kwic';
        }
    }
    $scope.showInfoIcon = function (val) {
        this.showIcon = val;
    }
    
    var getMWE = function (row, part_ind, ind) {
        
        var word = row.content[part_ind][ind].word;
        var merged = [];
        var index =  0;
        for (var i = 0; i < row.content.length; ++i) {
            merged = merged.concat (row.content[i]);
            if (i < part_ind)
                index += row.content[i].length;
            else if (i == part_ind)
                index += ind;
        }
        
        for (var i = index - 1; i >= 0; --i) {
            if (merged[i].mwe > -1)
                word = merged[i].word + ' ' + word;
            if (merged[i].mwe < 1)
                break;
        }
        for (var i = index + 1; i < merged.length; ++i) {
            if (merged[i].mwe > 0)
                word += ' ' + merged[i].word;
            else
                break;
            if (merged[i].mwe == 2)
                break;
        }
        return word;
    };

    $scope.getDefinition = function ($event, elem, ind, part_ind, row) {

        if (elem.mwe == -1 && elem.dif == 0)
            return;
        if (elem.mwe != -1)
            word = getMWE (row, part_ind, ind);
        else
            word = elem.lemma;
        $http.post ('backend/getDefinition.php', {word: word, mode: 'html'}).then (function (response) {
            var definition = response.data;
            var elem = angular.element ($event.currentTarget);
            var epos = elem.offset();
            var position_html = 'class="definition-box" style="top: ' + (epos.top + elem.height() + 2) + 'px; left: ' + (epos.left) + 'px"';
            var closing_tag = '<span ng-click="closeDefinition()" class="close-x">x</span>'
            var div_html = '<div id="definition" ' + position_html + '>' + closing_tag + definition + '</div>';
            var current_def = angular.element ('#definition');
            if (current_def.length > 0)
                current_def.remove ();
            angular.element ('body').append ($compile (div_html)($scope));
        }, function (response) {
        });
        // $scope.definition = '<b>Hasło</b> [wymowa]<br><br><i>Definicja</i><br><br>Etymologia';
    }

    $scope.closeDefinition = function () {
        var def = angular.element ('#definition');
        if (def.length > 0)
            def.remove ();
    }

    $scope.toggleShowMeta = function () {
        $scope.showMeta = !$scope.showMeta;
    }

    $scope.switchLayer = function () {
        for (var i = 0; i < layers.length; ++i)
            if (layers[i].name == $scope.layer.name) {
                if (i < layers.length - 1) {
                    $scope.layer = layers[i + 1];
                    if (i < layers.length - 2)
                        $scope.next_layer = layers[i + 2].name;
                    else
                        $scope.next_layer = layers[0].name;
                }
                else {
                    $scope.layer = layers[0];
                    $scope.next_layer = layers[1].name;
                }
                break;
            }
    }

    $scope.getAudioSrc = function (meta) {
        return './OUT/' + [meta.File, meta.From, meta.To].join ('-') + '.wav';
    }

    $scope.rrows = [1, 2, 3, 4];

    $scope.showPaginator = function () {
        if ($scope.currentSlice.length === 0 || $scope.pagination.numPerPage >= $scope.results.length) {
            return false;
        }
        else {
            return true;
        }
    }

    $scope.pageChanged = function () {
        $scope.currentSlice = getSlice ();
        // $scope.formattedSlice = formatSlice ();
        $scope.expanded.fill (0);
        window.scroll (0, 0);
    }

    $scope.limit = function (words, sectionNumber) {
            if (sectionNumber === 1)
                return words;
            else if (sectionNumber === 0) {
                if (words.length > limit)
                    return words.slice (words.length - limit);
                else
                    return words;
            }
            else {
                return words.slice (0, limit);
            }
        }

    $scope.expand = function (index) {
        $scope.expanded[index] = Math.pow ($scope.expanded[index] - 1, 2);
    }

    $scope.toggleSelection = function (index) {

        var recalculated = ($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage + index;
        $scope.selection[recalculated] = !$scope.selection[recalculated];
    }

    $scope.play = function ($event, meta) {
        var audio_path = $scope.getAudioSrc (meta);
        var elem = $event.currentTarget;
        if ($scope.playing == elem.id) {
            $scope.playing = '';
            angular.element (elem).removeClass ('fa-stop-circle').addClass ('fa-play-circle');
            $scope.audio.pause ();
        }
        else {
                if ($scope.playing != '') {
                    $scope.audio.pause ();
                    angular.element ('#' + $scope.playing).removeClass ('fa-stop-circle').addClass ('fa-play-circle');
                }
                $scope.audio = new Audio (audio_path);
                $scope.playing = elem.id;
                var el = angular.element (elem);
                $scope.audio.play ()
                .then (function () {
                    el.removeClass ('fa-play-circle').addClass ('fa-stop-circle');
                })
                .catch (function () {
                    el.removeClass ('fa-play-circle').addClass ('fa-times-circle');
                    $scope.playing = '';
                });
                $scope.audio.onended = function () {
                    var el = angular.element (elem).removeClass ('fa-stop-circle').addClass ('fa-play-circle')
                    $scope.playing = elem.id;
                }
            }
    }

    $scope.switchToContext = function ($event, index) {
        var place = ($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage + index;
        if ($scope.results[place].context === undefined) {
            var elem = angular.element (event.currentTarget);
            getContext (
                        {'from': $scope.results[place].meta.From,
                         'to': $scope.results[place].meta.To,
                         'file': $scope.results[place].meta.File},
                         place,
                         elem
                        );
        }
        else
            $scope.showContext[index] = !$scope.showContext[index];
    }

    $scope.toggleSelection = function (index) {

        var recalculated = ($scope.pagination.currentPage - 1) * $scope.pagination.numPerPage + index;
        $scope.selection[recalculated] = !$scope.selection[recalculated];
    }

    $scope.saveTSV = function (mode) {
        var toSave = [];
        var text = '';
		for (var mt in $scope.results[0].meta)
			text += mt + '\t';
		text += 'Left Context\tDMatch\tRight Context\n';
        var filename = 'results.tsv'
        if (mode === 'all')
            toSave = $scope.results;
        else
        {
            for (var i = 0; i < $scope.selection.length; ++i)
                if ($scope.selection[i] === true)
                    toSave.push ($scope.results[i]);
        }

        for (var i = 0; i < toSave.length; ++i) {
            var row = toSave[i];
			// if (i == 0)
			for (var mt in row.meta)
				text += row.meta[mt] + '\t';
            for (var j = 0; j < row.content.length; ++j) {
                    text += processResults.join (row.content[j], false);
                if (j < row.content.length - 1)
                    text += '\t';
                else
                    text += '\n';
                }
        }

        var blob = new Blob ([text], {type: 'text/text'});
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, filename);
        }
        else {
            var e = document.createEvent('MouseEvents'),
            a = document.createElement('a');
            a.download = filename;
            a.href = window.URL.createObjectURL(blob);
            a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
            e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            a.dispatchEvent(e);
            // window.URL.revokeObjectURL(url); // clean the url.createObjectURL resource
        }
    }

    /**     WATCHERS   */

    $scope.$watchCollection ('results', function () {
        if ($scope.results.length > 0)
        {
            $scope.currentSlice = getSlice ();
            $scope.expanded.fill (0);
        }
    });

    /** CONTROLLER FLOW **/

    processResults.get (true)
       .then (function (response) {
           var lines = response.data.split ("\n");
           $scope.query = lines[0].slice (0, lines[0].length - 2);
           $scope.numRes = lines[1];
           $scope.initials_loaded = true;
       }, function (response) {
           $scope.query = 'error :('
           $scope.numRes = 'error :('
       });
    $scope.results = processResults.get (false)
        .then (function (response) {
           $scope.results = processResults.prepare (response.data);
           // $scope.meta = corpusResponse.meta;
           $scope.currentSlice = getSlice ();
           // $scope.formattedSlice = formatSlice ();
           $scope.pagination.boundaries = $scope.results.length / $scope.pagination.numPerPage > 5;
           $scope.selection = new Array ($scope.results.length).fill (false);
           $scope.showContext = new Array ($scope.results.length).fill (false);
           $scope.initials_loaded = false;
           $scope.loaded = true;
           return response.data;
       }, function (response) {
           $scope.results = 'error :(' + response;
           return response;
       }
        );
}]);

spoco.directive( 'goClick', ['$location', function ( $location) {
  return function ( scope, element, attrs ) {
    var path;
    attrs.$observe( 'goClick', function (val) {
      path = val;
    });

    element.bind('click', function () {
      scope.$apply( function () {
        $location.path( path );
      });
    });
  };
}]);

spoco.directive('enterSubmit', ['$location', function ($location) {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                if (attrs.enterSubmit)
                    scope.$eval (attrs.enterSubmit, {event: event});
                else {
                    scope.$apply(function (){
                        $location.path ('/results');
                    });
                }

                event.preventDefault();
            }
        });
    };
}]);

spoco.directive('tooltip', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            element.hover(function(){
                // on mouseenter
                $timeout (function () {
                    element.tooltip('show')
                }, 400);
            }, function(){
                // on mouseleave
                element.tooltip('hide');
            });
        }
    };
}]);
