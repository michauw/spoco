spoco.directive('metadata', ['metaKeeper', 'gettextCatalog', function(metaKeeper, gettextCatalog) {
    return {
        templateUrl: 'jsapp/languageQuery/metadata/metadata.html',
        restrict: 'E',
        scope: {
            index: '=',
            place: '='
        },
        controller: function($scope, $rootScope, $window, gettextCatalog, gettext) {

            var getValues = function () {
                var values = metaKeeper.getValues ($scope.meta.name.slice (5));
                return values;
                values = [];
                metadata = metaKeeper.getMetaValues ();
                name = $scope.meta.name.slice (5);
                for (i = 0; i < metadata.length; ++i)
                    if (typeof metadata[i][name] != 'unknown')
                    {
                        var tmp = angular.copy (metadata)[i];
                        tmp['name'] = name;
                        tmp['id_field'] = metadata[i][name];
                        tmp['label'] = tmp[name];
                        values.push (tmp);
                    }
                values = values.slice().sort(compare).reduce(function(a,b){if (a.length == 0 || a.slice(-1)[0]['id_field'] !== b['id_field']) a.push(b);return a;},[]);
                return values;
            }

            $scope.meta = metaKeeper.get ($scope.index, $scope.place);

            $scope.getValues = function (negate) {
                return metaKeeper.getChoices ($scope.index, $scope.place, negate);
            }
            $scope.values = $scope.getValues ();

            // $scope.getMetaValues = function (){
            //     if ($scope.meta.name == 'meta_living-place')
            //         return check_matching (values, 'variety');
            //     else if ($scope.meta.name == 'meta_id')
            //         return check_matchin (values, 'living-place');
            //     return values;
            // };
            $scope.model = [];
            $scope.negation = false;
            $scope.$watch ('negation', function (newValue) {
                    metaKeeper.setMetaNegation ($scope.index, $scope.place, newValue);
                    $rootScope.$emit ('updateValues', newValue, $scope.meta.name.slice (5));
                    if ($scope.meta.name == 'meta_living-place')
                        $rootScope.$emit ('updateMarkers', $scope.negation);
            });
            $scope.multiSettings = {showCheckAll: false, buttonClasses: '"btn-success btn-sm meta"', dynamicTitle: true, scrollable: true, scrollableHeight: '150px', idProperty: 'id_field', checkBoxes: true};
            $scope.hint = {
							buttonDefaultText: gettextCatalog.getString($scope.meta.hint), 
							checkAll: gettextCatalog.getString('Zaznacz wszystko'),
							uncheckAll: gettextCatalog.getString('Odznacz wszystko'),
							selectionCount: gettextCatalog.getString('Wybrane: '),
							searchPlaceholder: gettextCatalog.getString('Szukaj...'),
							dynamicButtonTextSuffix: gettextCatalog.getString('Wybrane'),
							disableSearch: gettextCatalog.getString('Wyłącz szukanie'),
							enableSearch: gettextCatalog.getString('Włącz szukanie'),
							selectGroup: gettextCatalog.getString('Zaznacz wszystko'),
							allSelectedText: gettextCatalog.getString('Wszystko')
						   };
            $scope.$watchCollection ('model', function (newValue, oldValue) {
                metaKeeper.setMetaMulti ($scope.index, $scope.place, newValue);
                $rootScope.$emit ('updateValues', $scope.negation);
                if ($scope.meta.name == 'meta_living-place')
                {
                    $rootScope.$emit ('updateMarkers', $scope.negation);
                }
                //$scope.values = queryKeeper.getMetaChoices ($scope.index, $scope.place);
            });
            $rootScope.$on ('updateValues', function (event, negate) {
                $scope.values = $scope.getValues (negate);
            });
            $rootScope.$on ('updateLiving-Places', function (event) {
                if ($scope.meta.name == 'meta_living-place')
                {
                    var selections = metaKeeper.getMeta ($scope.index, $scope.place).multiValue;
                    $scope.model = [];
                    for (var i = 0; i < selections.length; ++i)
                        $scope.model.push ({id: selections[i]});
                }
            });
            $scope.$on ('gettextLanguageChanged', function (event) {
                $scope.hint = {
							buttonDefaultText: gettextCatalog.getString($scope.meta.hint), 
							checkAll: gettextCatalog.getString('Zaznacz wszystko'),
							uncheckAll: gettextCatalog.getString('Odznacz wszystko'),
							selectionCount: gettextCatalog.getString('Wybrane: '),
							searchPlaceholder: gettextCatalog.getString('Szukaj...'),
							dynamicButtonTextSuffix: gettextCatalog.getString('Wybrane'),
							disableSearch: gettextCatalog.getString('Wyłącz szukanie'),
							enableSearch: gettextCatalog.getString('Włącz szukanie'),
							selectGroup: gettextCatalog.getString('Zaznacz wszystko'),
							allSelectedText: gettextCatalog.getString('Wszystko')
						   };
            });
			$scope.$watch ('hint', function (nv) {	// needed for dropdown-multiselect to reflect language change
				$scope.$broadcast ('hintChanged', nv);
			});

        }
    };
}]);
