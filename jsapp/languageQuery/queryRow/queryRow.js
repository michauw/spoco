spoco.directive('queryRow', ['queryKeeper', 'ngDialog', function(queryKeeper, ngDialog) {
    return {
        templateUrl: 'jsapp/languageQuery/queryRow/queryRow.html',
        restrict: 'E',
        scope: {
            showTokensInBetween: '=',
            index: '=',
        },
        controller: function($scope, $rootScope) {

			var popUpOpened = false;
			var showWarning = true;

            $scope.queryRow = queryKeeper.get($scope.index);
            $scope.textFields = [];
            $scope.checkFields = [];
            for (var i = 0; i < $scope.queryRow.pattrs.length; ++i) {
                var attr = $scope.queryRow.pattrs[i];
                var obj = {'name': attr.name, 'hint': attr.hint, 'pos': i};
                if (attr.field == 'text')
                    $scope.textFields.push (obj);
                else if (attr.field == 'checkbox')
                    $scope.checkFields.push (obj);
            }
			$scope.focus = $scope.textFields[0].pos;
            $scope.colWidth = {
                                'md': Math.min (Math.floor (12 / $scope.textFields.length), 4), 
                                'sm': Math.min (Math.floor (6 / $scope.textFields.length), 2)
                              }
            $scope.$watch('queryRow', function(newValue, oldValue) {
				if (!showWarning)
					queryKeeper.setCqpChanged (false);
				if (queryKeeper.getCqpChanged() && !popUpOpened && showWarning)
				{
					popUpOpened = true;
					ngDialog.openConfirm({
						template: 'jsapp/languageQuery/queryRow/modal.html',
						className: 'ngdialog-theme-default'
					}).then(function (hide) {
						queryKeeper.setCqpChanged (false);
						queryKeeper.set($scope, $scope.index, newValue);
						popUpOpened = false;
						showWarning = !hide;
					}, function (hide) {
						queryKeeper.set($scope, $scope.index, oldValue);
						popUpOpened = false;
						showWarning = !hide;
					});
				}
				else
				{
					queryKeeper.set($scope.index, newValue);
				}
            }, true);
			$scope.getFields = function (){
				return queryKeeper.get($scope, $scope.index);
			}
			$scope.autocmp = function(ids, atype) {
				$scope.atype = atype;
				// getSuggestion (ids, atype, $scope);
			}
			// $scope.$watchCollection ('focus', function (nv) {
				// for (key in $scope.focus)
					// if ($scope.focus[key]) {
						// $scope.curModifiers = $scope.queryRow.modifiers[key];
						// console.log ('focus changed:', nv);
						// break;
					// }
			// });
        }
    };
}]);
