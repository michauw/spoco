spoco.factory('loadMetaData', ['$http', '$q', function(http, q) {

    return {
        getJsonFile: function(path) {
            var mdata;
            var deferred = q.defer();

            http.get(path).then (function(data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        }
    };
}]);


spoco.factory('queryKeeper', ['$rootScope', 'loadMetaData', function($rootScope, loadMetaData) {

    var queryRows = [];
    var pattrs = [];
    var metaFields = [];
    var metaValues = [];
    var cqpChanged = false;
    var cqlField = '';
    var filterGuardian = [];
    var limit = [];
    var varietyAdded = 0;
    var queryNegation = '';
	var landingQuery = '';

    var match = function (value, possibleValues, key) {
            var found = false;
            needle = value[key];
            for (var i = 0; i < possibleValues.length; ++i)
                if (needle == possibleValues[i])
                {
                    found = true;
                    break;
                }
            if (found) {
                return true;
            }
            return false;
        };

    var compare = function (a,b) {
                if (a.label > b.label)
                    return 1;
                if (a.label < b.label)
                    return -1;
                return 0;
            }
            
    var basicStateQR = function () {
        var obj = {
                       from: '',
                       to: '',    
                };
        var rowPattrs = [];
        pattrs.forEach (function (attr) {
            var copy = Object.assign ({}, attr);
            if (attr.field != 'checkbox')
                copy.modifiers = {beginWith: '', endWith: '', caseSensitive: false};
            rowPattrs.push (copy);
        });
            
        obj['pattrs'] = rowPattrs;
        
        return obj;
    }

    return {

        clear: function() {
            for (i = 0; i < queryRows.length; ++i)
            {
                for (j = 0; j < pattrs.length; ++j)
                    if (pattrs[j].hasOwnProperty ('initialValue'))
                        pattrs[j].value = pattrs[j].initialValue;
                    else if (pattrs[j].field == 'checkbox')
                        queryRows[i].pattrs[j].value = false;
                    else {
                        queryRows[i].pattrs[j].value = '';
                        queryRows[i].pattrs[j].modifiers = {beginWith: false, endWith: false, caseSensitive: false};
                    }
            }
            for (i = 0; i < metaFields.length; ++i)
                metaFields[i].value = '';
        },

        get: function(i) {
            return queryRows[i];
        },
        getAll: function() {
            return queryRows;
        },        
        getCqpChanged: function () {
            return cqpChanged;
        },   
		getLandingQuery: function () {
			return landingQuery;
		},        
        getLimit: function () {
            return limit;
        },        
        getMeta: function(i, place) {
            return metaFields[i * 3 + place];
        },
        getMetaAll: function() {
            return metaFields;
        },
        getMetaLength: function() {
            return metaFields.length - varietyAdded;
        },
        getMetaValues: function() {
            return metaValues;
        },
        getNegation: function() {
            return queryNegation;
        },        
        getPattrs: function () {
            return pattrs;
        },
        getRowsNumber: function () {
            return queryRows.length;
        },
        forceLoadMV: function () {
            loadMetaData.getMetaFile('settings/meta_data.json').then(function(response) {
            metaValues = angular.fromJson(response);
            metaValuesLoaded = true;
            });
        },
        isMetaLoaded: function () {
            return metaValuesLoaded;
        },
        length: function() {
            return queryRows.length;
        },
        metaLength: function() {
            return metaFields.length - varietyAdded;
        },        
        pop: function() {
            if (queryRows.length > 1) queryRows.pop();
        },
        push: function() {
            if (!(queryRows instanceof Array)) {
                queryRows = [];
            }
            queryRows.push(basicStateQR ());
        },    
		resetQuery: function () {
			landingQuery = '';
            queryRows = [];
		},        
        set: function(index, item) {
            if (!(queryRows instanceof Array)) {
                queryRows = [];
            }
            queryRows[index] = item;
            return item;
        },
        setCqpChanged: function (value) {
            if (value === '') {
                cqpChanged = false;
                cqlField = '';
            }
            else {
                cqpChanged = true;
                cqlField = value;
            }
        },    
		setLandingQuery: function (query) {
			landingQuery = query;
		},        
        setLimit: function (index, place, value) {
            limit = [];
            for (i = 0; i < value.length; ++i)
                limit.push (value[i]);
        },        
        setMeta: function(i, place, item) {
            metaFields[i * 3 + place] = item;
            return item;
        },
        setMetaVariety: function (variety) {
            for (var i = metaFields.length - 1; i >= 0; --i)
                if (metaFields[i].name == 'meta_variety')
                    break;
            if (i < 0)
                return;
            var isSet = metaFields[i].multiValue.indexOf (variety);
            if (isSet > -1)
                    metaFields[i].multiValue.splice (isSet, 1);
                else
                    metaFields[i].multiValue.push (variety);
            $rootScope.$emit ('updateValues');
            return isSet;
        },
        setNegation: function(value) {
            queryNegation = value;
        },
        setPAttrs: function (pos_attr) {
            pattrs = [];
            pos_attr.forEach (function (attr) {
                if (attr.hasOwnProperty ('initialValue'))
                    attr.value = attr.initialValue;
                else if (attr.field == 'checkbox')
                    attr.value = false;
                else
                    attr.value = '';
               pattrs.push (attr);
            });
        },
        setQuery: function (place, value, field) {
            if (place >= queryRows.length) {
                place = queryRows.length;
                this.push ()
            }
            this.update (place, field, value);
        },
        update: function (index, key, value) {
            queryRows[index][key] = value;
        },
        prepareQuery: function() {
			if (landingQuery !== '') {
				return landingQuery;
			}
            if (cqpChanged)
                return cqlField;
            var outputQuery = [];
            var parsedQueryRows = [];
            for (i = 0; i < queryRows.length; ++i) {
                if (typeof(queryRows[i]) == 'undefined') continue;
                var values = [];
                queryRows[i].pattrs.forEach (function (attr) {
                    if (attr.field == 'text')               
                        values.push (attr.value.trimEnd ().split (' '));
                    else
                       values.push (new Array (attr.value));
                });
                var maxlen = Math.max.apply (Math, values.map (function (x) {return x.length}));
                var tmpRows = new Array (maxlen);
                for (var j = 0; j < maxlen; ++j) {
                    tmpRows[j] = basicStateQR ();
                    for (var k = 0; k < tmpRows[j].pattrs.length; ++k) {
                        if (tmpRows[j].pattrs[k].field != 'checkbox') {
                            tmpRows[j].pattrs[k].modifiers = queryRows[i].pattrs[k].modifiers;
                            if (j < values[k].length)
                                tmpRows[j].pattrs[k].value = values[k][j];
                        }
                        else
                            tmpRows[j].pattrs[k].value = values[k][0];
                    };
                }
                parsedQueryRows = parsedQueryRows.concat (tmpRows);
            }
            for (var row of parsedQueryRows) {
                if (typeof(row) == 'undefined')
                    continue;
                var currentRowQuery = [];                 
                for (var attr of row.pattrs) {
                    var value = '';
                    if (attr.field == 'text' && attr.value)
                    {
                        value = attr.value;
                        if (attr.modifiers.endWith)
                            value = '.*' + value;
                        if (attr.modifiers.beginWith)
                            value += '.*';
                    }
                    else if (attr.field == 'checkbox') {
                        if (attr.value) {
                            if (attr.hasOwnProperty ('valueOn'))
                                value = attr.valueOn;
                        }
                        else if (attr.hasOwnProperty ('valueOff'))
                            value = attr.valueOff;
                    }
                    if (value) {
                        if (attr.modifiers.caseSensitive)
                            value += '%c';
                        currentRowQuery.push (attr.name + '="' + value + '"');
                    }
                }                 

                if (currentRowQuery.length > 0) {
                    outputQuery.push ('[' + currentRowQuery.join (' & ') + ']');
                    if (row.from < row.to && row.to > 0) {
                        outputQuery.push ('[]{' + (row.from) + ',' + (row.to) + '}');
                    }
                }
            }
            
            outputQuery = outputQuery.join ('').replace (/"/g, "'");
            return outputQuery;

        }
    }
}]);


spoco.factory('configData', ['gettext', function(gettext) {

    var langs = {
         bg: gettext("Bulgarian"),
         bul: gettext("Bulgarian"),
         be: gettext("Belorussian"),
         bel: gettext("Belorussian"),
         cs: gettext("Czech"),
         cz: gettext("Czech"),
         ces: gettext("Czech"),
         hr: gettext("Croatian"),
         hrv: gettext("Croatian"),
         mk: gettext("Macedonian"),
         mkd: gettext("Macedonian"),
         pl: gettext("Polish"),
         pol: gettext("Polish"),
         ru: gettext("Russian"),
         rus: gettext("Russian"),
         sk: gettext("Slovak"),
         slk: gettext("Slovak"),
         sl: gettext("Slovene"),
         slv: gettext("Slovene"),
         sr: gettext("Serbian"),
         srp: gettext("Serbian"),
         uk: gettext("Ukrainian"),
         ukr: gettext("Ukrainian"),
         us: gettext("Upper Sorbian"),
         da: gettext("Danish"),
         dan: gettext("Danish"),
         de: gettext("German"),
         deu: gettext("German"),
         en: gettext("English"),
         eng: gettext("English"),
         nl: gettext("Dutch"),
         nld: gettext("Dutch"),
         no: gettext("Norwegian"),
         nor: gettext("Norwegian"),
         sv: gettext("Swedish"),
         swe: gettext("Swedish"),
         es: gettext("Spanish"),
         spa: gettext("Spanish"),
         fr: gettext("French"),
         fra: gettext("French"),
         it: gettext("Italian"),
         ita: gettext("Italian"),
         pt: gettext("Portuguese"),
         por: gettext("Portuguese"),
         ro: gettext("Romanian"),
         ron: gettext("Romanian"),
         lt: gettext("Lithuanian"),
         lit: gettext("Lithuanian"),
         lv: gettext("Latvian"),
         lav: gettext("Latvian"),
         et: gettext("Estonian"),
         ese: gettext("Estonian"),
         el: gettext("Greek"),
         ell: gettext("Greek"),
         eo: gettext("Esperanto"),
         epo: gettext("Esperanto"),
         fi: gettext("Finnish"),
         fin: gettext("Finnish"),
         hu: gettext("Hungarian"),
         hun: gettext("Hungarian"),
         hy: gettext("Armenian"),
         hye: gettext("Armenian")
     };

    return {
        getLangs: function (lang) {
            return langs[lang];
        }
    };
}]);

spoco.factory ('processResults', ['$http', 'queryKeeper', 'metaKeeper', function ($http, queryKeeper, metaKeeper) {
    var results_retrieved = false;
    return {
                get: function (onlyInitials, location) {
                    var corpus = 'main';
                    var contextSize = 1;
                    var script = 'backend/get_results.php';
                    var query = queryKeeper.prepareQuery ();
                    if (query != '')
                        query += metaKeeper.addToQuery ();
                    if (location !== undefined) {
                        contextSize = 3;
                        corpus = 'full';
                        script = 'backend/get_context.php';
                    }
                    params = {query: query, corpus: corpus, meta: '', location: location, init: onlyInitials, contextSize: contextSize};

                    return $http.post (script, params);
                    },
                processLine: function (line, pattrs, isContext = false) {

                    var sticky_left = [',', '.', '?', '!', ':', ';', ')', ']', '>', '%', '$'];
                    var sticky_right = ['(', '[', '<'];
                    var stick_r = false;
                    var matching_section = false;
                    var meta = this.getMeta (line);
                    var words = [];
                    var flat_words = line.trim().split (' ');
                    var match_beg = -1;
                    var match_end = flat_words.length;
                    var row;
                    var mwe = -1;
                    var diffs = [];

                    for (var j = 0; j < flat_words.length; ++j)
                    {
                        var check_mwe = /<(\/)?mwe>/.exec (flat_words[j]);
                        if (check_mwe !== null)
                        {
                            flat_words[j] = flat_words[j].replace (check_mwe[0], '');
                            mwe = (check_mwe[1] === '/') ? 2 : 0;
                        }
                        else if (mwe == 0 || mwe == 1)
                        {
                            mwe = 1;
                        }
                        else {mwe = -1;}

                        var elements = flat_words[j].split ('/');
                        if (elements.length != pattrs.length)
                            continue;
                        var word = {
                                    'type': 'word',
                                    'mwe': mwe
                                   };
                        var cwb_order = pattrs.map (function (attr) {return attr.cwb});
                        elements = cwb_order.map (function (ind) {return elements[ind]});    // reorder CWB output to match attributes sequence                                   
                        for (var k = 0; k < elements.length; ++k)
                            word[pattrs[k].name] = elements[k];
                        if (word.dif != 0) {
                            diffs.push (word.word);
                        }
                        var first = pattrs[cwb_order.indexOf (0)].name;
                        var first_val = word[first]
                        if (first_val[0] === '<' && first_val !== '<unknown>' && first_val.length > 1)
                        {
                            word[first] = first_val.slice (1);
                            matching_section = true;
                            match_beg = words.length;
                        }
                        word['match'] = matching_section;
                        var last = pattrs[cwb_order.indexOf (Math.max.apply (null, cwb_order))].name; // name of the element with the highest 'cwb' value
                        var last_val = word[last]
                        if (last_val !== '<unknown>' && last_val.length > 1 && last_val[last_val.length -1] == '>')
                        {
                            word[last] = last_val.slice (0, last_val.length - 1);
                            matching_section = false;
                            match_end = words.length;
                        }
                        if (j === 0 || stick_r || 
							(first.length == 1  && sticky_left.indexOf (first) != -1) ||
							word['tag'].startsWith ('aglt')
							)
                            word['space'] = false;
                        else {
                            word['space'] = true;
                        }
                        if (first.length == 1 && sticky_right.indexOf (first) != -1)
                            stick_r = true;
                        else
                            stick_r = false;
                        // if (matching_section === 'true')
                        words.push (word);
                    }
                    if (!isContext) {
                        var left = words.slice (0, match_beg);
                        var match = words.slice (match_beg, match_end + 1);
                        var right = words.slice (match_end + 1);
                        row = [left, match, right];
                    }
                    else {
                        row = words;
                    }

                    return {'row': row, 'meta': meta};
                },
                getMeta: function (line) {
					var meta = {};
                    // meta['utterance_id'] = parseInt (line.match (/^\s*\d+/)[0]);
					var meta_match = line.match (/: <.*?>:/);
					if (!meta_match)
						return {};
					var meta_part = meta_match[0].slice (2, -1);
					var meta_pat = /<([a-z]+)_([^ ]+) (.*?)>/g;
					while ((match = meta_pat.exec (meta_part)) != null) {
                        if (match[1] == 'utterance' && match[2] == 'id')
                            meta['utterance_id'] = match[3];
                        else
						    meta[this.splitAndCapitalize (match[2])] = match[3];
					}
					return meta;
				},
                join: function (words, kwicit) {

                    // take all the words and join them into one line


                    var sticky_left = [',', '.', '?', '!', ':', ';', ')', ']', '%', '$'];
                    var sticky_right = ['(', '['];
                    var space = '';
                    var current = '';
                    var kwic = [];
                    var stick_r = false;
                    var word = '';;
                    for (var i = 0; i < words.length; ++i) {
                        if (words[i]['word'] === '')
                            continue;
                        word = words[i]['word'];
                        if (kwicit && words[i]['matchstart'] === true) {
                            kwic.push (current);
                            current = '';
                        }
                        if (i == 0 || stick_r || (word.length == 1  && sticky_left.indexOf (word) != -1))
                            space = '';
                        else
                            space = ' ';
                        if (word.length == 1 && sticky_right.indexOf (word) != -1)
                            stick_r = true;
                        else
                            stick_r = false;
                        current += space + word;
                        if (kwicit && words[i]['matchend'] === true) {
                            kwic.push (current);
                            current = '';
                        }
                    }
                    kwic.push (current);

                    return kwic;
                },
                prepare: function (data) {

                    var rows = [];

                    var lines = data.split ("\n");
                    var pattrs = queryKeeper.getPattrs ();

                    for (var i = 0; i < lines.length; ++i) {
                        var data = this.processLine (lines[i], pattrs);
                        rows.push ({'content': data.row, 'meta': data.meta})
                    }

                    // results_retrieved = true;
                    return rows;
                },
                prepareContext: function (data) {

                    var context = [];
                    var lines = data.split ("\n");
                    var currentSpeaker = '';
                    var index;
                    var pattrs = queryKeeper.getPattrs ();

                    for (var i = 0; i < lines.length; ++i) {
                        var lineData = this.processLine (lines[i], pattrs, isContext = true);
                        if (currentSpeaker != '' && currentSpeaker != lineData.meta.Spkr) {
                            context.push ([{type: 'break'}]);
                        }
                        if (currentSpeaker != lineData.meta.Spkr) {
                            context.push ([{type: 'speaker', 'spkr': lineData.meta.Spkr}]);
                        }
                        currentSpeaker = lineData.meta.Spkr;
                        context.push (lineData.row);
                        if (i == Math.floor (lines.length / 2)) {
                            index = context.length - 1;
                        }
                        if (currentSpeaker != 'Eksplorator')
                            context.push ([{type: 'audio', 'File': lineData.meta.File, 'From': lineData.meta.From, 'To': lineData.meta.To}]);
                    }

                    return {'context': context, 'index': index};
                },
                getResultsRetrieved: function () {
                    return results_retrieved;
                },
                splitAndCapitalize: function (string) {

                    string = string.replace (/_+/g, ' ');
                    return string.charAt (0).toUpperCase () + string.slice (1);
                }

    }
}]);

spoco.factory ('metaKeeper', function () {

    var metaFields = [];
    var metaValues = [];
    var queryNegation;

    var compare = function (a,b) {
                if (a.label > b.label)
                    return 1;
                if (a.label < b.label)
                    return -1;
                return 0;
            }

    var match = function (value, possibleValues, key) {
                var found = false;
                needle = value[key];
                for (var i = 0; i < possibleValues.length; ++i)
                    if (needle == possibleValues[i])
                    {
                        found = true;
                        break;
                    }
                if (found) {
                    return true;
                }
                return false;
            };

    return {
        addToQuery: function() {

            var metaList = [];
            for (i = 0; i < metaFields.length; ++i)
            {
                if (metaFields[i].value != '')
                    metaList.push ('match.' + metaFields[i].name + "='" + metaFields[i].value + "'");
                else if (metaFields[i].multiValue != []) {
                    tmpList = [];
                    var operators = ["='", ' | '];
                    if (metaFields[i].negate == true)
                        operators = ["!='", ' & '];
                    for (j = 0; j < metaFields[i].multiValue.length; ++j)
                        tmpList.push ('match.' + metaFields[i].name + operators[0] + metaFields[i].multiValue[j] + "'");
                    if (tmpList.length != 0)
                    {
                        statement = tmpList.join (operators[1]);
                        if (tmpList.length > 1)
                            statement = '(' + statement + ')';
                        metaList.push (statement);
                    }
                }
            }
            if (metaList.length != 0) return '::' + metaList.join(' & ');

            return '';
        },
            clear: function () {
                metaFields = [];
            },
            get: function (i, place) {
                return metaFields[i * 3 + place];
            },
            getAllFields: function() {
                return metaFields;
            },
            getChoices: function (index, place, negation = false) {
                var constraints = {}
                var choices = [];
                if (index == -1)
                    name = 'living-place';
                else
                    name = metaFields[index * 3 + place].name.slice (5);
                for (var i = 0; i < metaFields.length; ++i)
                    if (typeof metaFields[i].multiValue != 'undefined' && metaFields[i].multiValue.length > 0)
                        constraints[metaFields[i].name.slice (5)] = [metaFields[i].multiValue, metaFields[i].negate];
                for (var i = 0; i < metaValues.length; ++i)
                {
                    isMatch = true;
                    for (c in constraints)
                    {
                        if (c == name)
                            continue;
                        if (match (metaValues[i], constraints[c][0], c) == constraints[c][1])
                        {
                            isMatch = false;
                            break;
                        }
                    }
                    if ((isMatch) && (metaValues[i][name] !== ""))
                    {
                        tmp = {}
                        tmp['id_field'] = metaValues[i][name];
                        tmp['label'] = metaValues[i][name];
                        choices.push (tmp);
                    }
                }
                var ch = choices.slice().sort(compare).reduce(function(a,b){if (a.length == 0 || a.slice(-1)[0]['id_field'] !== b['id_field']) a.push(b);return a;},[]);
                return choices.slice().sort(compare).reduce(function(a,b){if (a.length == 0 || a.slice(-1)[0]['id_field'] !== b['id_field']) a.push(b);return a;},[]);
            },
            getNames: function () {
                var names = [];
                for (var i = 0; i < metaFields.length; ++i)
                    names.push (metaFields[i].name);

                return names;
            },
            getValues: function (name) {
                var values = [];
                for (var i = 0; i < metaValues.length; ++i)
                    if (metaValues[i][name] != undefined && metaValues[i][name] != '')
                        values.push (metaValues[i][name]);
                return values;
            },
            getLength: function() {
                return metaFields.length;
            },
            set: function(i, place, item) {
                metaFields[i * 3 + place] = item;
                return item;
            },
            setFields: function (fields) {
                metaFields = fields;
            },
            setMetaNegation: function(i, place, value) {
                metaFields[i * 3 + place].negate = value;
            },
            addMetaMulti: function (i, place, item) {
                metaFields[i * 3 + place].multiValue.push (item.id);
                return item;
            },
            removeMetaMulti: function (i, place, item) {
                var index = metaFields[i * 3 + place].multiValue.indexOf (item.id);
                if (index > -1)
                {
                    metaFields[i * 3 + place].multiValue.splice (index, 1);
                    return item;
                }
                return;
            },
            setMetaMulti: function (i, place, item) {
                metaFields[i * 3 + place].multiValue = [];
                for (j = 0; j < item.length; ++j)
                {
                    metaFields[i * 3 + place].multiValue.push (item[j].id_field);

                }
                return item;
            },
            setValues: function (values) {
                metaValues = values;
            }
    }
});

spoco.factory ('dictKeeper', function () {
	var currentQuery = {};
	var currentEntry = '';
	var scope = null;
	return {
		get: function (type) {
			if (type == 'entry')
				return currentEntry;
			else if (type == 'query')
				return currentQuery;
			else if (type == 'scope')
				return scope;
			else 
				return '';
		},
		set: function (type, value) {
			if (type == 'query') 
				currentQuery = value;
			else if (type == 'entry') {
				currentEntry = value;
			}
			else if (type == 'scope') {
				scope = value;
			}
		}
	}
});