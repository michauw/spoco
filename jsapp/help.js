spoco.controller ('help', ['$scope', '$timeout', 'ngDialog', 'helpBoxes', function ($scope, $timeout, ngDialog, helpBoxes) {

    var removeLineBreaks = function (text) {
                    var op = false;
                    var newText = '';
                    for (var i = 0; i < text.length; ++i) {
                        if (text[i] == '"') {
                            op = !op;
                            newText += text[i];
                        }
                        else if (text[i] == '\n')
                            if (op)
                                newText += '\\n';
                            else
                                newText += text[i];
                        else
                            newText += text[i];
                    }
                    return newText;
                };
    helpBoxes = angular.fromJson (removeLineBreaks (helpBoxes));
    $scope.scrollTo = function (id) {
        var el = document.getElementById(id);
        el.scrollIntoView ();
        var header_height = $('nav').height ();
        var scrolledY = window.scrollY;
        if(scrolledY){
            window.scroll(0, scrolledY - header_height - 10);
        }
        el = angular.element (el);
        el.addClass ('highlight');
        $timeout (function () {
            el.removeClass ('highlight');
        }, 600);
    }
    $scope.scrollBottom = function (id = null) {
        window.scroll (0, document.body.scrollHeight);
        if (id) {
            var elem = angular.element ('#' + id);
            elem.addClass ('highlight');
            $timeout (function () {
                elem.removeClass ('highlight');
            }, 600);
        }
        
    }
    $scope.showQueryPage = function () {
        ngDialog.open ({
            template: 'jsapp/pages/help/queryPage.html',
            width: '85%',
            resolve: {
                helpBoxes: function () {
                    return helpBoxes;
                }
            },
            className: 'ngdialog-theme-help',
            controller: 'cheatSheet',
            showClose: false
        });
    };
    $scope.showResultsPage = function () {
        $scope.show = 'plain';
        ngDialog.open ({
            template: 'jsapp/pages/help/resultsPagePlain.html',
            width: '85%',
            resolve: {
                helpBoxes: function () {
                    return helpBoxes;
                }
            },
            className: 'ngdialog-theme-help',
            controller: 'cheatSheet',
            showClose: false
        });
    };

}]);

spoco.controller ('cheatSheet', ['$scope', '$compile', '$timeout', 'helpBoxes', function ($scope, $compile, $timeout, helpBoxes) {
                $scope.show = 'plain';
                var curCoords = '';
                var curPromise = null;
                $scope.showHelp = function ($event, val, permanent = false) {
                    if (curPromise) {
                        $timeout.cancel (curPromise);
                    };
                    var desc = helpBoxes[val];
                    $event.preventDefault ();
                    console.log ('event position:', $event.pageX, $event.pageY);
                    if (permanent)
                        $scope.closeHelp (true);
                    var elem = angular.element ($event.currentTarget);
                    var coords = elem.attr ('coords');
                    // console.log ('coords:', coords);
                    if (curCoords == coords)
                        return;
                    if (permanent)
                        curCoords = coords;
                    coords = coords.split (',');
                    var offset = elem.offset ();
                    // console.log ('offset:', offset);
                    var epos = {top: offset.top + parseInt (coords[3]) + 15 - window.scrollY, left: offset.left + parseInt (coords[0]) + 20};
                    // console.log ('epos:', epos);
                    // console.log ('elem, height:', elem, elem.height ());
                    var position_html = 'class="definition-box' + (permanent ? ' perm' : '') + '" style="top: ' + (epos.top + 2) + 'px; left: ' + (epos.left) + 'px"';
                    var closing_tag = '<span ng-click="closeHelp(true)" class="close-x ml-2' + (permanent ? '' : ' disabled') + '">x</span>'
                    var div_html = '<div id="description" ' + position_html + '>' + closing_tag + desc + '</div>'
                    var current_def = angular.element ('#description');
                    var dialog = angular.element ('.ngdialog-theme-help');
                    // console.log ('div html:', div_html);
                    // console.log ('div html:', div_html);
                    if (!permanent)
                        curPromise = $timeout (function () {
                            dialog.append ($compile (div_html)($scope));
                        }, 200);
                    else 
                        dialog.append ($compile (div_html)($scope));
            }
            
            $scope.closeHelp = function (permanent = false) {
                var toClose = '.definition-box' + (permanent ? '' : ':not(.perm)');
                var def = angular.element (toClose);
                if (def.length > 0)
                    def.remove ();
                if (permanent)
                    curCoords = '';
                if (curPromise)
                    $timeout.cancel (curPromise);
            }
            
            $scope.scrollTo = function (id) {
                $scope.closeThisDialog ();
                var el = document.getElementById(id);
                el.scrollIntoView ();
                var header_height = $('nav').height ();
                var scrolledY = window.scrollY;
                if(scrolledY){
                    window.scroll(0, scrolledY - header_height - 10);
                }
                el = angular.element (el);
                el.addClass ('highlight');
                $timeout (function () {
                    el.removeClass ('highlight');
                }, 600);
            }
            $scope.switchPlainKwic = function (val) {
                $scope.show = val;
            }
            $scope.close = function () {
                $scope.closeThisDialog ();
            }
}]);