/*
* rwdImageMaps AngularJS Directive v1.0
*
* Allows image maps to be used in a responsive design by recalculating the area coordinates to match the actual image size on load and window.resize
* 
* Original Copyright (c) 2013 Matt Stow
* https://github.com/stowball/jQuery-rwdImageMaps
* http://mattstow.com
* Licensed under the MIT license
*
* angular-rwdImageMaps.js (by Philip Saa)
* https://github.com/cowglow/
* @cowglow
*
* adapted for SpoCo needs (Michal Wozniak - michauwww@gmail.com)
*/

angular.module('rwdImageMaps',[])
	.directive('rwdimgmap', function($window){
		return{
			restrict: 'CA',
			link:function(scope, element, attrs){
				element.bind('load', function() {
                    var size = element.attr('size').split(',');
					var w = parseInt(size[0]),
						h = parseInt(size[1]);
						
					function resize(){
						if (!w || !h) {
							var temp = new Image();
							temp.src = $(element).attr('src');
							if(temp.src == undefined)
								temp.src = $(element).attr('ng-src');

							if (!w)
								w = temp.width;
							if (!h)
								h = temp.height;
						}
						
						var curW = $(element).width(),
							curH = $(element).height(),
							map = attrs.usemap.replace('#', ''),
                            wPercent = $(element).width()/100,
							hPercent = $(element).height()/100,
							c = 'coords';
                        ratioW = curW / w;
                        ratioH = curH / h;
						angular.element('map[name="' + map + '"]').find('area').each(function(){
							var $this = $(this);
							
							if (!$this.data(c)){
								$this.data(c, $this.attr(c));
							}
								
							var coords = $this.data(c).split(','),
                                coordsPercentOld = new Array(coords.length),
								coordsPercent = new Array(coords.length);
							
							for (var i = 0; i<coordsPercent.length; ++i){
								if (i % 2 === 0){
									coordsPercent[i] = parseInt(coords[i] * ratioW);
								} else {
									coordsPercent[i] = parseInt(coords[i] * ratioH);
								};
							};
                            for (var i = 0; i<coordsPercent.length; ++i){
								if (i % 2 === 0){
									coordsPercentOld[i] = parseInt(((coords[i]/w)*100)*wPercent);
								} else {
									coordsPercentOld[i] = parseInt(((coords[i]/h)*100)*hPercent);
								};
							};
							$this.attr(c, coordsPercent.toString());
						});
					}
					angular.element($window).resize(resize).trigger('resize');
				});
			}
		};
	});