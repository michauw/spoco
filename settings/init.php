<?php
/**
 * all initial information, paths, globals	
 * created on 16.02.2007 by Roland Meyer
 */

// $OS = "macosx"

/* Locations */

$CWBDIR = "";
$PARCORPUSDIR = "/srv/data/Corpus/";
$REGISTRY = "/srv/data/Corpus/Registry";
$CORPUSNAME = "SPISZ";
$CORPUSNAME_FULL = "SPISZFULL";
$PATTR_PATH = __DIR__ . "/pattrs.json";
$METAPATH = __DIR__ . "/meta.json";
$METADATA = __DIR__ . "/meta_data.json";

/* Positional attributes */

$pattrs = json_decode (file_get_contents ($PATTR_PATH));

/* Metadata */

$metastructure = json_decode (file_get_contents ($METAPATH));
$ANNOTCONTEXT = [];
foreach ($metastructure as $field)
    array_push ($ANNOTCONTEXT, $field->name);
$ANNOTCONTEXT = join  (", ", $ANNOTCONTEXT);

/* Various settings */
$DEBUG = true;
$ENCODING = "UTF-8";
?>
